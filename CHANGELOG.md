# CHANGELOG

+ v0.10
  + Voreinstellung `jump_height` auf 30 herunter gesetzt.
  + Roboter Status (Online/Offline/Error) wird nun alle 3 sek. dem Broker mitgeteilt.
  + Status vom IRSwitch und den digitalen IO's werden zum Broker geschickt.

+ v0.9
  + `getIrSwitch()` und `getColor()` pub. die Werte sofort zum MQTT-Broker.
  + Fehler bei `getIrSwitch()` behoben. Zuvor hat die Methode immer True zurückgegeben.
  + Fehler behoben bei `setConveyorPort()`. Fehlermeldung war, dass der Port nicht mit "STEPPER" anfängt (#2).
  + Die Verarbeitung der MQTT Topics war in Windows fehlerhaft.
  + DobotMqtt hört nun auf alle `msg` Nachrichten von Allen.

+ v0.8
  + Paho-MQTT Lib eingebunden
  + DobotMqtt impl. [#1]
  + Alle Methoden, die ein Port als Param. haben (`setConveyorPort()`, `setIrSwitch()`, usw.) prüfen nun ob die richtige Port-Gruppe (STEPPER bzw. GP) angegeben wurde.
  + Klassen `DobotColorSensor`, `DobotEndEffectorParams` impl.
    + Rückgabe von `getColor()`, `getEndEffector()` entsprechend angepasst.
  + Werzeuge Saugnapf und Stift neu vermessen.

+ v0.7
    - CHANGELOG.md hinzugefügt.
    - INSTALL.md hinzugefügt.
    - Fehler bei `getColor()` korrigiert.
    - `jumpTo()`, `jumpRel()` und `jumpToWithL()` um `r` (Rotation) Parameter erweitert.
    - Die Klasse `DobotPose` kann nun auch ohne Daten initialisiert werden (Vorgabe f. xyzr = 0).
    - Abfrage der ID bei der init. der Dobot Klasse entfernt. Bei neuen Dobots führte das zum Ausnahmefehlern, da Daten vom Dobot fehlerhaft sind.
    - `setConveyorSpeed()` korrigiert. Negative Werte waren nicht möglich.