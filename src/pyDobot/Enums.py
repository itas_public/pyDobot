#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
pyDobot - Pure python3 api for the dobot magician.
Copyright (C) 2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
"""

__author__      = "Y. Korte-Wagner"
__email__       = "yves.korte-wagner@jade-hs.de"
__copyright__   = "2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS"
__licence__     = "GPLv2"
__version__ = "0.10"

from enum import Enum, IntEnum, IntFlag

class DobotMessageCtrl(IntFlag):
    """Control Flag der Dobot Nachrichten.

    Es bestimmt, ob vom/zum Dobot gelesen/geschrieben und/oder
    die Nachricht in die Queue gelegt werden soll.
    """
    #: lesen
    NULL                    = 0x00
    #: schreiben
    RW                      = 0x01
    #: zur Warteschlange hinzufügen
    ISQUEUE                 = 0x02

class DobotMessageID(Enum):
    """Eine Auflistung der Nachrichten von und zum Dobot.

    Die Auflistung beinhaltet ein leserlichen Namen der Nachricht, die ID,
    das Control Flag und wie der Payload der Nachricht interpretiert werden soll.
    """
    def __new__(cls, value:int, ctrl:DobotMessageCtrl, fmt:str) -> object:
        obj         = object.__new__(cls)
        obj._value_ = (value, ctrl & DobotMessageCtrl.RW)    # isQueue Flag entfernen
        obj.id      = value
        obj.ctrl    = ctrl
        obj.fmt     = fmt
        return obj

    # NAME                      = (ID, CTRL_FLAG, Param Format)
    UNDEFINED                   = (-1, DobotMessageCtrl.NULL, None)
    # Device Info
    GET_DEVICE_SN               = (0, DobotMessageCtrl.NULL, None)
    GET_DEVICE_NAME             = (1, DobotMessageCtrl.NULL, None)
    GET_DEVICE_VERION           = (2, DobotMessageCtrl.NULL, None)
    GET_DEVICE_WITH_L           = (3, DobotMessageCtrl.NULL, "<B")      # isWithL
    SET_DEVICE_WITH_L           = (3, DobotMessageCtrl.RW, "<BB")       # isWithL, version
    GET_DEVICE_ID               = (5, DobotMessageCtrl.NULL, None)
    # Real-Time Pose
    GET_POSE                    = (10, DobotMessageCtrl.NULL, "<8f")    # x. y. z. r. j1-j4
    GET_POSE_L                  = (13, DobotMessageCtrl.NULL, "<f")     # poseL
    # Alarm
    GET_ALARM_STATE             = (20, DobotMessageCtrl.NULL, "bv")     # bit vector
    CLEAR_ALL_ALARMS_STATE      = (20, DobotMessageCtrl.RW, None)
    # Homing
    GET_HOME_PARAMS             = (30, DobotMessageCtrl.NULL, "<4f")    # x, y, z, r
    SET_HOME_PARAMS             = (30, DobotMessageCtrl.RW, "<4f")
    SET_HOME_CMD                = (31, DobotMessageCtrl.RW, None)
    # Endeffector
    GET_ENDEFFECTOR_PARAMS      = (60, DobotMessageCtrl.NULL, "<3f")    # 3x float
    SET_ENDEFFECTOR_PARAMS      = (60, DobotMessageCtrl.RW, "<3f")
    GET_ENDEFFECTOR_SUCTION_CUP = (62, DobotMessageCtrl.NULL, "<2B")    # 2x uint8
    SET_ENDEFFECTOR_SUCTION_CUP = (62, DobotMessageCtrl.RW, "<2B")
    GET_ENDEFFECTOR_GRIPPER     = (63, DobotMessageCtrl.NULL, "<2B")
    SET_ENDEFFECTOR_GRIPPER     = (63, DobotMessageCtrl.RW, "<2B")
    # PTP
    GET_PTP_JOINT_PARAMS        = (80, DobotMessageCtrl.NULL, "<8f")    # j1Vel-j4Vel, j1Acc-j4Acc
    SET_PTP_JOINT_PARAMS        = (80, DobotMessageCtrl.RW, "<8f")
    GET_PTP_COORDINATE_PARAMS   = (81, DobotMessageCtrl.NULL, "<4f")    # xyzVel, rVel, xyzAcc, rAcc
    SET_PTP_COORDINATE_PARAMS   = (81, DobotMessageCtrl.RW, "<4f")
    SET_PTP_CMD                 = (84, DobotMessageCtrl.RW, "<B4f")     # mode, x, y, z, r
    GET_PTP_L_PARAMS            = (85, DobotMessageCtrl.NULL, "<2f")    # vel, acc
    SET_PTP_L_PARAMS            = (85, DobotMessageCtrl.RW, "<2f")
    SET_PTP_CMD_WITH_L          = (86, DobotMessageCtrl.RW, "<B5f")     # mode, x, y, z, r, l
    # EIO
    GET_IO_MULTIPLEXING         = (130, DobotMessageCtrl.NULL, "<2B")   # adress, multiplex
    SET_IO_MULTIPLEXING         = (130, DobotMessageCtrl.RW, "<2B")
    GET_IO_DO                   = (131, DobotMessageCtrl.NULL, "<B?")   # address, state
    SET_IO_DO                   = (131, DobotMessageCtrl.RW, "<B?")
    GET_IO_PWM                  = (132, DobotMessageCtrl.NULL, "<Bff")  # address, freq, dutyCycle
    SET_IO_PWM                  = (132, DobotMessageCtrl.RW, "<Bff")
    GET_IO_DI                   = (133, DobotMessageCtrl.NULL, "<B?")   # address, state
    GET_IO_ADC                  = (134, DobotMessageCtrl.NULL, "<BH")   # uint8, uint16
    SET_EMOTOR                  = (135, DobotMessageCtrl.RW, "<BBi")    # index, state, speed
    GET_COLOR_SENSOR            = (137, DobotMessageCtrl.NULL, "<3B")   # r, g, b
    SET_COLOR_SENSOR            = (137, DobotMessageCtrl.RW, "<3B")     # state, port, version
    GET_IR_SWITCH               = (138, DobotMessageCtrl.NULL, "<B")    # state
    SET_IR_SWITCH               = (138, DobotMessageCtrl.RW, "<3B")     # state, port, version
    # Queued exec control
    SET_QUEUED_CMD_START_EXEC   = (240, DobotMessageCtrl.RW, None)
    SET_QUEUED_CMD_CLEAR        = (245, DobotMessageCtrl.RW, None)
    GET_QUEUED_CMD_CURRENT_INDEX= (246, DobotMessageCtrl.NULL, "<Q")    # index

class DobotAlarmState(Enum):
    """Eine Auflistung der Fehlercodes vom Dobot.
    """
    # Common Error
    ERR_COMMON_MIN                  = 0x0
    ERR_COMMON_RESET                = ERR_COMMON_MIN
    ERR_COMMON_UNDEF_INSTRCTION     = 0x01
    ERR_COMMON_FS_ERROR             = 0x02
    ERR_COMMON_MCU_FPGA_COM_ERROR   = 0x03
    ERR_COMMON_ANGLE_SENSOR_ERROR   = 0x04
    ERR_COMMON_MAX                  = 0x0f

    # Plan Error
    ERR_PLAN_MIN                    = 0x10
    ERR_PLAN_INV_SINGULARITY        = ERR_PLAN_MIN
    ERR_PLAN_INV_CALC               = 0x11
    ERR_PLAN_INV_LIMIT              = 0x12
    ERR_PLAN_PUSH_DATA_REPEAT       = 0x13
    ERR_PLAN_ARC_INPUT_PARAM        = 0x14
    ERR_PLAN_JUMP_PARAM             = 0x15
    ERR_PLAN_MAX                    = 0x1f

    # Move Error
    ERR_MOVE_MIN                    = 0x20
    ERR_MOVE_INV_SINGULARITY        = ERR_MOVE_MIN
    ERR_MOVE_INV_CALC               = 0x21
    ERR_MOVE_INV_LIMIT              = 0x22
    ERR_MOVE_MAX                    = 0x2f

    # Over Speed Error
    ERR_OVERSPEED_MIN               = 0x30
    ERR_OVERSPEED_AXIS1             = ERR_OVERSPEED_MIN
    ERR_OVERSPEED_AXIS2             = 0x31
    ERR_OVERSPEED_AXIS3             = 0x32
    ERR_OVERSPEED_AXIS4             = 0x33
    ERR_OVERSPEED_MAX               = 0x3f

    # Limit Error
    ERR_LIMIT_MIN                   = 0x40
    ERR_LIMIT_AXIS1_POS             = ERR_LIMIT_MIN
    ERR_LIMIT_AXIS1_NEG             = 0x41
    ERR_LIMIT_AXIS2_POS             = 0x42
    ERR_LIMIT_AXIS2_NEG             = 0x43
    ERR_LIMIT_AXIS3_POS             = 0x44
    ERR_LIMIT_AXIS3_NEG             = 0x45
    ERR_LIMIT_AXIS4_POS             = 0x46
    ERR_LIMIT_AXIS4_NEG             = 0x47
    ERR_LIMIT_AXIS23_POS            = 0x48
    ERR_LIMIT_AXIS23_NEG            = 0x49
    ERR_LIMIT_MAX                   = 0x4f

    # Lost Step
    ERR_LOSE_STEP_MIN               = 0x50,
    ERR_LOSE_STEP_AXIS1             = ERR_LOSE_STEP_MIN,
    ERR_LOSE_STEP_AXIS2             = 0x51
    ERR_LOSE_STEP_AXIS3             = 0x52
    ERR_LOSE_STEP_AXIS4             = 0x53
    ERR_LOSE_STEP_MAX               = 0x5f

    def toStr(err_code:int) -> str:
        """Gibt eine detailierte Nachricht zum Fehlercode zurück.

        :param err_code: Fehlercode vom Dobot.
        :type err_code: int
        :return: Detailierte Nachricht des Fehlers.
        :rtype: str
        """
        errStr = {
            DobotAlarmState.ERR_COMMON_RESET: "The alarm will be triggered after resetting system",
            DobotAlarmState.ERR_COMMON_UNDEF_INSTRCTION: "Receive undefined instruction",
            DobotAlarmState.ERR_COMMON_FS_ERROR: "Filesystem error",
            DobotAlarmState.ERR_COMMON_MCU_FPGA_COM_ERROR: "There is a failed communication between MCU and FPGA when system is initializing",
            DobotAlarmState.ERR_COMMON_ANGLE_SENSOR_ERROR: "Get an error value of angle sensor",

            DobotAlarmState.ERR_PLAN_INV_SINGULARITY: "The target point is in abnormal position in Cartesian coordinate system",
            DobotAlarmState.ERR_PLAN_INV_CALC: "The target point is out of the workspace, that causes inverse resolve alarm",
            DobotAlarmState.ERR_PLAN_INV_LIMIT: "The inverse resolve of target point is out of the limitation.",

            DobotAlarmState.ERR_MOVE_INV_LIMIT: "The motion is out of the limitation when Dobot Maigican moves"
            # ...
        }

        return "{}(0x{:02x}) -> {}".format(
            DobotAlarmState(err_code),
            err_code.value,
            errStr.get(err_code, "[No error string avaiable]")
        )

class DobotPort(Enum):
    """Eine Auflistung der Ports vom Dobot.
    """
    def __new__(cls, value:int, name:str) -> object:
        obj         = object.__new__(cls)
        obj._value_ = (value, name)
        obj.id      = value
        obj.pname   = name
        return obj

    GP1     = (0, "GP1")
    GP2     = (1, "GP2")
    GP4     = (2, "GP4")
    GP5     = (3, "GP5")

    STEPPER1= (0, "STEPPER1")
    STEPPER2= (1, "STEPPER2")

    EIO0    = (0, "EIO0")
    EIO1    = (1, "EIO1")
    EIO2    = (2, "EIO2")
    EIO3    = (3, "EIO3")
    EIO4    = (4, "EIO4")
    EIO5    = (5, "EIO5")
    EIO6    = (6, "EIO6")
    EIO7    = (7, "EIO7")
    EIO8    = (8, "EIO8")
    EIO9    = (9, "EIO9")
    EIO10   = (10, "EIO10")
    EIO11   = (11, "EIO11")
    EIO12   = (12, "EIO12")
    EIO13   = (13, "EIO13")
    EIO14   = (14, "EIO14")
    EIO15   = (15, "EIO15")
    EIO17   = (17, "EIO17")
    EIO18   = (18, "EIO18")
    EIO19   = (19, "EIO19")
    EIO20   = (20, "EIO20")

class DobotIOFunction(IntEnum):
    """Eine Auflistung der möglichen Funktionen eines EIO's.
    """
    IO_FUNC_DUMMY       = 0
    IO_FUNC_DO          = 1
    IO_FUNC_PWM         = 2
    IO_FUNC_DI          = 3
    IO_FUNC_ADC         = 4

class DobotDeviceVersion(IntEnum):
    """Eine Auflistung der Hardware-Versionen.
    """
    VER_V1  = 0
    VER_V2  = 1

class DobotPTPMode(IntEnum):
    """Eine Auflistung der Bewegungsarten.
    """
    MOVJ    = 1
    MOVL    = 2


if __name__ == "__main__":
    p = DobotPort.STEPPER1
    print(p.name)