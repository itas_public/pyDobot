#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
pyDobot - Pure python3 api for the dobot magician.
Copyright (C) 2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
"""

__author__      = "Y. Korte-Wagner"
__email__       = "yves.korte-wagner@jade-hs.de"
__copyright__   = "2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS"
__licence__     = "GPLv2"
__version__ = "0.10"

import logging

def setupLogger() -> None:
    # File logger
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(levelname)s] %(name)s: %(message)s",
        filename="pyDobot.log",
        filemode="w"
    )

    # INFO Nachrichten in die Konsole
    c_hdl = logging.StreamHandler()
    c_hdl.setLevel(logging.INFO)
    # c_hdl.setLevel(logging.getLogger("").getEffectiveLevel())
    c_fmt = logging.Formatter("[%(levelname)s] %(name)s: %(message)s")
    c_hdl.setFormatter(c_fmt)
    logging.getLogger("").addHandler(c_hdl)

    return