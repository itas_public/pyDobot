#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
pyDobot - Pure python3 api for the dobot magician.
Copyright (C) 2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
"""

__author__      = "Y. Korte-Wagner"
__email__       = "yves.korte-wagner@jade-hs.de"
__copyright__   = "2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS"
__licence__     = "GPLv2"
__version__ = "0.10"

# intern
import logging
import time
import threading
from typing import Union
# extern
import serial
# own
from .Messages import *


class DobotSerial(threading.Thread):
    def __init__(self, port:str = "/dev/ttyUSB0") -> None:
        """Kommunikation mit dem Dobot über eine serielle Schnittstelle.

        Übernimmt das (de)codieren der Nachrichten und das Send und Empfangen von Nachrichten
        über die serielle Schnittstelle

        :param port: Port des Dobots ("z.B. "COM3" unter Windows oder "/dev/ttyUSB0 unter Linux), defaults to "/dev/ttyUSB0"
        :type port: str, optional
        :raise serial.SerialException: wenn Port nicht geöffnet werden konnte
        """
        super().__init__()
        self.name           = "DobotCommunicationThread"
        #: aktueller Queue Index vom Dobot
        self.current_q_idx  = 0
        self.read_fails     = 0
        self.alarms         = None

        self._ev_exit   = threading.Event()
        self._lock      = threading.RLock()
        self._logger    = logging.getLogger("{}.{}".format(__name__, self.__class__.__name__))

        self._dev           = serial.Serial()
        self._dev.baudrate  = 115200
        self._dev.port      = port
        self._dev.parity    = serial.PARITY_NONE
        self._dev.stopbits  = serial.STOPBITS_ONE
        self._dev.bytesize  = serial.EIGHTBITS
        self._dev.rts       = False
        self._connect()

        if not self._dev.isOpen():
            raise IOError("Can not open serial port!")

        self.start()

    def close(self) -> None:
        """Verbindung zum Dobot schließen
        """
        self._logger.info("close connection...")
        self._ev_exit.set()
        if self.is_alive():
            self.join()
        self._dev.close()
        return

    def sendCommand(self, msg:DobotMessage, wait:bool=False) -> Union[None, DobotMessage]:
        """Eine Nachricht zum Dobot senden.

        :param msg: Nachricht die versendet werden soll
        :type msg: DobotMessage
        :param wait: Programm blockieren bis Nachticht verarbeitet worden ist, defaults to False
        :type wait: bool, optional
        :return: Antwort-Nachricht. None wenn keine Nachricht zurück gesendet wurde.
        :rtype: Union[None, DobotMessage]
        """
        # wenn auf ein befehl gewartet werden soll, muss ISQUEUE flag gesetzt sein.
        if wait and not (msg.ctrl & DobotMessageCtrl.ISQUEUE):
            msg.ctrl |= DobotMessageCtrl.ISQUEUE
        elif not wait and (msg.ctrl & DobotMessageCtrl.ISQUEUE):
            msg.ctrl &= ~DobotMessageCtrl.ISQUEUE

        self._lock.acquire()
        self._logger.debug("send: {}".format(msg))

        # Workaround f. Windows
        resp_msg = None
        while resp_msg is None:
            if self.read_fails >= 5:
                raise serial.SerialException()
            self._write(msg)
            time.sleep(0.10)
            resp_msg = self._read()

        self._lock.release()
        self._logger.debug("recv: {}".format(resp_msg))

        if wait:
            while resp_msg.q_idx != self.current_q_idx:
                time.sleep(0.25)

        return resp_msg

    def run(self) -> None:
        """Wird ausgeführt wenn der Thread gestartet wird.
        """
        self._ev_exit.clear()
        while not self._ev_exit.is_set():
            # aktuellen queue index
            cIdx = self.sendCommand(DobotMessage(DobotMessageID.GET_QUEUED_CMD_CURRENT_INDEX))
            if cIdx:
                self.current_q_idx = int(cIdx.params[0])
            time.sleep(0.25)

            # alarm abfragen
            #: ToDo überarbeiten
            aStates = self.sendCommand(DobotMessage(DobotMessageID.GET_ALARM_STATE))
            if aStates:
                # Ausgabe in Console wenn Alarm existiert
                if aStates.params:
                    self.alarms = aStates.params
                    for err in aStates.params:
                        self._logger.error(DobotAlarmState.toStr(err))
            else:
                self.alarms = None

            time.sleep(0.25)

        return

    def _connect(self) -> None:
        """Verbindung zum Dobot aufbauen.
        """
        self._dev.open()
        return

    def _read(self) -> Union[None, DobotMessage]:
        """Lese von serieller Verbindung.

        :return: Eine DobotMessage wenn eine gelesen werden konnte, None sonst.
        :rtype: Union[None, DobotMessage]
        """
        if not self._dev.isOpen():
            self._logger.warning("Serial connection not open!")
            return None

        buf = None
        while not buf:
            buf = self._dev.read_all()
            self._logger.debug("<- {}".format(asHexString(buf)))
            if not buf:
                self._logger.warning("Got no data from robot! Reconnect in 3s...")
                # Workaround f. Windows: reconnect.
                self.read_fails += 1
                self._dev.close()
                time.sleep(3)
                self._connect()
                return None

        if buf[:2] != bytes([0xaa, 0xaa]):
            self._logger.error("wrong sync!")
            return None

        msg = DobotMessage.fromBytes(buf)
        return msg

    def _write(self, msg:DobotMessage) -> None:
        """Schreibe Nachricht zu Dobot über serielle Schnittstalle.

        :param msg: DobotMessage die gesendet werden soll
        :type msg: DobotMessage
        """
        if not self._dev.isOpen():
            self._logger.warning("Serial connection closed!")
            return

        b = msg.toBytes()
        self._logger.debug("-> {}".format(asHexString(b)))
        self._dev.write(b)

        return



if __name__ == "__main__":
    pass