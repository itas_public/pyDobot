#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
pyDobot - Pure python3 api for the dobot magician.
Copyright (C) 2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
"""

__author__      = "Y. Korte-Wagner"
__email__       = "yves.korte-wagner@jade-hs.de"
__copyright__   = "2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS"
__licence__     = "GPLv2"
__version__ = "0.10"

# intern
import logging
from random import random
import struct
from typing import Union, Any
# extern
from BitVector import BitVector
# own
from . import Logger
from .Enums import DobotMessageID, DobotMessageCtrl, DobotAlarmState


def asHexString(buf:bytearray) -> str:
    return " ".join( ["0x{:02x}".format(b) for b in buf] )


class DobotMessage(object):
    def __init__(self, msg_id:DobotMessageID) -> None:
        self.msg_name   = msg_id.name
        self.msg_id     = msg_id.id
        self.ctrl       = msg_id.ctrl
        self.params_fmt = msg_id.fmt
        self.params_bin = bytearray()
        self.params     = None
        self.q_idx      = None
        self.length     = 2
        self.csum       = None

        self._logger    = logging.getLogger("{}.{}".format(__name__, self.__class__.__name__))

    def __str__(self) -> str:
        return "<Message(): {}:{}, flags:{}, q_idx:{}, params:{}>".format(self.msg_name, self.msg_id, str(self.ctrl), self.q_idx, self.params)

    def __repr__(self) -> str:
        return self.__str__()

    def toBytes(self) -> bytearray:
        """Serialisiert die gesamte Nachticht.

        :return: Die Nachticht als Bytearray.
        :rtype: bytearray
        """
        if self.msg_id == DobotMessageID.UNDEFINED.id:
            self._logger.error("Unknow message type!")
            return bytearray()
        self.binParams()
        self.length = len(self.params_bin) + 2
        self.csum   = self.calcChecksum()

        # sync länge id flags ...
        data = bytearray([0xaa, 0xaa, self.length, self.msg_id, self.ctrl.value])
        # ... payload ...
        data.extend(self.params_bin)
        # ... prüfsumme
        data.append(self.csum)

        return data

    def calcChecksum(self) -> int:
        R = self.msg_id + self.ctrl.value
        for p in self.params_bin:
            R += p
        R = R % 256
        return (256 - R) % 256

    def parseParams(self) -> None:
        """Interpretiert die binären Daten in self.params_bin und erstellt eine tuple in self.params.

        :return: _description_
        :rtype: _type_
        """
        self.params = None
        self.q_idx = None

        if not self.params_bin:
            return  # keine binär daten zum parsen

        # bei ISQUEUE Flag wird der index der queue im roboter als param zurückgegeben
        if self.ctrl & DobotMessageCtrl.ISQUEUE:
            self.q_idx = struct.unpack("<Q", self.params_bin)[0]
            return

        if self.params_fmt:
            # als bitvector
            if self.params_fmt == "bv":
                buf = list(self.params_bin)
                buf.reverse()   # LSB -> MSB
                bv = str(BitVector(rawbytes = bytes(buf)).reverse())
                self.params = tuple([DobotAlarmState(idx) for idx in range(len(bv)) if bv.startswith("1", idx)])
                return
            # binär
            try:
                self.params = struct.unpack(self.params_fmt, self.params_bin)
            except struct.error:
                self._logger.error("error while parsing payload data")
                self.params = None
        else:
            self.params = self.params_bin

        return

    def binParams(self) -> None:
        self.params_bin = bytearray()

        if not self.params:
            return  # keine params vorhanden

        if self.params_fmt == "bv":
            return  # todo? wird eigentlich nie gebraucht

        if self.params_fmt:
            self.params_bin = bytearray(struct.pack(self.params_fmt, *self.params))
        else:
            self.params_bin = bytearray(self.params)

        return

    @staticmethod
    def fromBytes(b) -> Union[Any, None]:
        # sync prüfen
        if b[:2] != bytes([0xaa, 0xaa]):
            logging.error("wrong sync!")
            return None

        m               = DobotMessage(DobotMessageID( (b[3], DobotMessageCtrl(b[4]) & DobotMessageCtrl.RW) ))
        m.ctrl          = DobotMessageCtrl(b[4])
        m.length        = b[2]
        m.params_bin    = b[5:5+m.length-2]

        recv_csum       = b[5+m.length-2]

        if recv_csum != m.calcChecksum():
            logging.error("wrong checksum!")
            return None

        m.csum = recv_csum
        m.parseParams()

        # Todo: überarbeiten, daten sollen nicht verloren gehen
        if len(b) != 5+m.length-1:
            logging.warning("Some bytes left while parsing message. maybe some messages will be lost.")

        return m



def _tester():
    logging.basicConfig(level=logging.DEBUG)

    m1 = DobotMessage(DobotMessageID(1))
    print(m1)

    m2 = DobotMessage.fromBytes(m1.toBytes())
    print(m2)

    m3 = DobotMessage(DobotMessageID.GET_POSE)
    m3.params_bin = struct.pack("<f", random())
    m3.parseParams()
    print(m3)
    print(asHexString(m3.params_bin))

    m4 = DobotMessage.fromBytes(m3.toBytes())
    print(m4)
    print(asHexString(m4.params_bin))

    return



if __name__ == "__main__":
    _tester()