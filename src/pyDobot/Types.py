#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
pyDobot - Pure python3 api for the dobot magician.
Copyright (C) 2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
"""

__author__      = "Y. Korte-Wagner"
__email__       = "yves.korte-wagner@jade-hs.de"
__copyright__   = "2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS"
__licence__     = "GPLv2"
__version__ = "0.10"

from typing import Union, Any
from collections import namedtuple

class DobotException(Exception):
    pass

class _DobotType():
    fields = ("",) * 4

    def __init__(self, data:Union[list, tuple]=(0, 0, 0, 0)) -> None:
        """Basisklasse für weitere DobotTypes.

        Zählbares namedtuple durch implm. von __iter__().

        :param data: Werte der Felder.
        :type data: Union[list, tuple]
        """
        self._data_cls  = namedtuple(self.__class__.__name__, self.fields)
        self._data      = self._data_cls(*data)
        self._n         = 0

    def asDict(self) -> dict:
        out = {}
        for f in self.fields:
            out[f] = getattr(self, f)
        return out


    def __getattribute__(self, name:str) -> Any:
        if name in object.__getattribute__(self, "fields"):
            return object.__getattribute__(self._data, name)
        return object.__getattribute__(self, name)

    def __setattr__(self, name:str, value:Any) -> None:
        if name in object.__getattribute__(self, "fields"):
            d = {name:value}
            self._data = self._data._replace(**d)
        object.__setattr__(self, name, value)

    def __str__(self) -> str:
        return "<{}>".format(self._data)

    def __repr__(self) -> str:
        return self.__str__()

    def __iter__(self) -> Any:
        return self

    def __next__(self) -> Any:
        if self._n >= len(self._data):
            self._n = 0
            raise StopIteration
        else:
            self._n += 1
            return self._data[self._n-1]


class DobotPose(_DobotType):
    fields = ("x", "y", "z", "r")

class DobotJointAngles(_DobotType):
    fields = ("j1", "j2", "j3", "j4")

class DobotColorSensor(_DobotType):
    fields = ("r", "g", "b")

class DobotEndEffectorParams(_DobotType):
    fields = ("xBais", "yBais", "zBais")

class DobotPTPJointParams(_DobotType):
    fields = ("vel", "acc")

class DobotCoordinateParams(_DobotType):
    fields = ("vel", "acc")

class DobotPTPLParams(_DobotType):
    fields = ("vel", "acc")



if __name__ == "__main__":
    x = DobotPose([1,2,3,4])

    x.r = 10
    print(*x)