#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
pyDobot - Pure python3 api for the dobot magician.
Copyright (C) 2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
"""

__author__      = "Y. Korte-Wagner"
__email__       = "yves.korte-wagner@jade-hs.de"
__copyright__   = "2022 Y. Korte-Wagner, Jade Hochschule Oldenburg TGM/ITAS"
__licence__     = "GPLv2"
__version__ = "0.10"

# intern
import threading
import time
import math
from collections import namedtuple
from pathlib import PurePosixPath
import json
from queue import LifoQueue
# extern
import paho.mqtt.client as mqtt
# own
from . import Logger
Logger.setupLogger()
from .Messages import *
from .Communication import DobotSerial
from .Types import *
from .Enums import *

class Dobot():
    ENDEFFECTOR_NONE            = [0, 0, 0]
    # ENDEFFECTOR_SUCTIONCUP      = [59.7, 0, 0]    # alt
    ENDEFFECTOR_SUCTIONCUP      = [10.48, 0, 68.79] # neu
    # ENDEFFECTOR_PEN             = [61, 0, 0]      # alt
    ENDEFFECTOR_PEN             = [11.3, 0, 80.23]  # neu

    ENDEFFECTOR_GRIPPER         = [59.7, 0, 0]      # alt

    # Konstanten
    _CONVEYOR_STEP_PER_CIRCLE   = 32000
    _CONVEYOR_MM_PER_CIRCLE     = math.pi * 36.0

    jump_height                 = 30

    def __init__(self, port:str) -> None:
        """Dobot Instanz erstellen

        :param port: COM-Port, an den der Dobot angeschlossen ist. Z.B. "COM3" unter Windows oder "/dev/ttyUSB0" unter Linux.
        :type port: str
        """
        self.conveyor_port  = DobotPort.STEPPER1

        self._logger        = logging.getLogger("{}.{}".format(__name__, self.__class__.__name__))

        self._ev_exit       = threading.Event()
        self._dev           = DobotSerial(port=port)

        self._dev.sendCommand(DobotMessage(DobotMessageID.SET_QUEUED_CMD_START_EXEC))
        self._dev.sendCommand(DobotMessage(DobotMessageID.SET_QUEUED_CMD_CLEAR))

        # dev_id              = self._dev.sendCommand(DobotMessage(DobotMessageID.GET_DEVICE_ID))  exception von decode() bei neueren Dobots
        dev_name            = self._dev.sendCommand(DobotMessage(DobotMessageID.GET_DEVICE_NAME))
        dev_version         = self._dev.sendCommand(DobotMessage(DobotMessageID.GET_DEVICE_VERION))
        dev_sn              = self._dev.sendCommand(DobotMessage(DobotMessageID.GET_DEVICE_SN))

        self._logger.info("Dobot:")
        self._logger.info("\tName: {}".format(dev_name.params.decode()))
        # self._logger.info("\tID: {}".format(dev_id.params.decode()))
        self._logger.info("\tVersion: {}".format(dev_version.params.decode()))
        self._logger.info("\tSerialNr: {}".format(dev_sn.params.decode()))


    def close(self) -> None:
        """Verbindung zum Dobot schließen.
        """
        self._dev.close()
        return


    # ALARMS

    def clearAlarms(self) -> None:
        """Alle Fehlermeldungen vom Dobot quitieren.
        """
        self._dev.alarms = None
        self._dev.sendCommand(DobotMessage(DobotMessageID.CLEAR_ALL_ALARMS_STATE))
        return

    def getAlarms(self) -> tuple[DobotAlarmState]:
        """Fehlerspeicher vom Dobot lesen

        :return: Eine tuple mit allen AlarmState des Dobots.
        :rtype: tuple
        """
        # r = self._dev.sendCommand(DobotMessage(DobotMessageID.GET_ALARM_STATE))
        # return r.params
        return self._dev.alarms


    # HOMING

    def home(self) -> None:
        """Homing ausführen.

            Diese Methode muss immer dann ausgeführt werden, sobald der Arm bei der
            Bewegung behintert wurde und die Positionen nicht mehr stimmen.
        """
        self._dev.sendCommand(DobotMessage(DobotMessageID.SET_HOME_CMD), wait=True)
        return


    # CONFIG

    def setJumpHeight(self, new_jump_h:float) -> None:
        """Höhe für jump*() Befehle setzen.

        :param new_jump_h: Z in [mm]
        :type new_jump_h: float
        """
        self.jump_height = new_jump_h

    def setDeviceWithL(self, withL:bool=True, version:DobotDeviceVersion=DobotDeviceVersion.VER_V2) -> None:
        """Linearschine (de)aktivieren.

        :param withL: Gibt an ob Linearschine existiert, defaults to True
        :type withL: bool, optional
        :param version: Version der Linearschine, defaults to DobotDeviceVersion.VER_V2
        :type version: DobotDeviceVersion, optional
        """
        m               = DobotMessage(DobotMessageID.SET_DEVICE_WITH_L)
        m.params        = (int(withL), version.value)
        self._dev.sendCommand(m)
        return


    def setEndEffector(self, endeffector_bias:list) -> None:
        """Werkzeug (Endeffector) am Dobot festlegen.

        Dabei wird die xyz-Verschiebung durch das Werkzeug am Ende des Arms angegeben.

        :param endeffector_bias: Eine Liste der Form [x y z].
        :type endeffector_bias: list
        """
        assert len(endeffector_bias)==3, "need a list with 3 element for endeffector_bias"
        m               = DobotMessage(DobotMessageID.SET_ENDEFFECTOR_PARAMS)
        m.params        = tuple(endeffector_bias)
        self._dev.sendCommand(m)
        return

    def getEndEffector(self) -> DobotEndEffectorParams:
        """Aktuelle Verschiebung aufgrund des Werkzeugs abrufen.

        :return: Verschiebung in xyz entsprechend dem Werkzeug.
        :rtype: DobotEndEffectorParams
        """
        m               = DobotMessage(DobotMessageID.GET_ENDEFFECTOR_PARAMS)
        resp_msg        = self._dev.sendCommand(m)
        # endeff_cls      = namedtuple("DobotEndEffectorParams", ["xBias", "yBias", "zBias"])
        # endeff          = endeff_cls(*resp_msg.params)
        return DobotEndEffectorParams(resp_msg.params)


    def setHomeParams(self, pose:DobotPose) -> None:
        """Home-Position setzen.

        :param pose: DobotPose der neuen Home-Position.
        :type pose: DobotPose
        """
        m           = DobotMessage(DobotMessageID.SET_HOME_PARAMS)
        m.params    = tuple(pose.x, pose.y, pose.z, pose.r)
        self._dev.sendCommand(m, wait=False)
        return

    def getHomeParams(self) -> DobotPose:
        """Aktuelle Home-Position abrufen.

        :return: DobotPose der aktuellen Home-Position.
        :rtype: DobotPose
        """
        m           = DobotMessage(DobotMessageID.GET_HOME_PARAMS)
        resp_msg    = self._dev.sendCommand(m)
        return DobotPose(resp_msg.params)


    def setPTPJointParams(self, vel:list, acc:list) -> None:
        """Geschwindigkeit und Beschleunigung für Armbewegungen über Winkel setzen.

        Diese Parameter werden nur angewendet, wenn der Arm über Winkelwerte angesteuert wird.

        :param vel: Eine Liste mit den Geschw. für J1-J4.
        :type vel: list
        :param acc: Eine Liste mit den Beschl. für J1-J4.
        :type acc: list
        :raise AssertionError: Wenn einer der Listen eine ungülige Anzhal an Werten besitzt.
        """
        assert len(vel)==4, "need a list with 4 element for vel"
        assert len(acc)==4, "need a list with 4 element for acc"
        m           = DobotMessage(DobotMessageID.SET_PTP_JOINT_PARAMS)
        m.params    = tuple(vel + acc)
        self._dev.sendCommand(m)
        return

    def getPTPJointParams(self) -> namedtuple:
        """Aktuelle Geschw. und Beschl. bei Ansteuerung über Winkel der Arm-Motoren abrufen

        :return: Geschw. und Beschl. der einzelnen Arm-Motoren.
        :rtype: namedtuple

        :todo: Soll Klasse DobotJParams erzeugen.
        """
        m           = DobotMessage(DobotMessageID.GET_PTP_JOINT_PARAMS)
        resp_msg    = self._dev.sendCommand(m)
        jParam_cls  = namedtuple("DobotJParams", ["vel", "acc"])
        jParam      = jParam_cls(resp_msg.params[:4], resp_msg.params[4:])
        return jParam


    def setPTPCoordinateParams(self, xyzrVel:list, xyzrAcc:list) -> None:
        """Geschwindigkeit und Beschleunigung für Armbewegungen über kartesische Koordinaten setzen.

        Diese Parameter werden nur angewendet, wenn der Arm über kartesische Koordinaten angesteuert wird.

        :param xyzrVel: Eine Liste der Form [xyz, r], wobei xyz Geschwindigkeit über zyz ist und r die Endeffektor Geschw.
        :type xyzrVel: list
        :param xyzrAcc: Eine Liste der Form [xyz, r], wobei xyz Beschl. über zyz ist und r die Endeffektor Beschl.
        :type xyzrAcc: list
        :raise AssertionError: Wenn einer der Listen eine ungülige Anzhal an Werten besitzt.
        """
        assert len(xyzrVel)==2, "need a list with 2 element for xyzrVel"
        assert len(xyzrAcc)==2, "need a list with 2 element for xyzrAcc"
        m           = DobotMessage(DobotMessageID.SET_PTP_COORDINATE_PARAMS)
        m.params    = tuple(xyzrVel + xyzrAcc)
        self._dev.sendCommand(m)
        return

    def getPTPCoordinateParams(self) -> namedtuple:
        """Geschwindigkeit und Beschleunigung für Armbewegungen über kartesische Koordinaten abrufen.

        :return: _description_
        :rtype: namedtuple
        """
        m           = DobotMessage(DobotMessageID.GET_PTP_COORDINATE_PARAMS)
        resp_msg    = self._dev.sendCommand(m)
        cParam_cls  = namedtuple("DobotCoordianteParams", ["vel", "acc"])
        cParam      = cParam_cls(resp_msg.params[:2], resp_msg.params[2:])
        return cParam


    def setPTPLParams(self, vel:float, acc:float) -> None:
        m           = DobotMessage(DobotMessageID.SET_PTP_L_PARAMS)
        m.params    = (float(vel), float(acc))
        self._dev.sendCommand(m)
        return

    def getPTPLParams(self) -> namedtuple:
        m           = DobotMessage(DobotMessageID.GET_PTP_L_PARAMS)
        resp_msg    = self._dev.sendCommand(m)
        params_cls  = namedtuple("DobotPTPLParams", ["vel", "acc"])
        return params_cls(*resp_msg.params)


    # POSE

    def getPose(self) -> DobotPose:
        """Aktuelle Koordinaten.

        :return: Ein DobotPose Objekt mit den aktuellen Koordinaten
        :rtype: DobotPose
        """
        resp_msg    = self._dev.sendCommand(DobotMessage(DobotMessageID.GET_POSE))
        # pose_cls = namedtuple("DobotPose", ["x", "y", "z", "r"])
        # pose = pose_cls(*resp_msg.params[:4])
        return DobotPose(resp_msg.params[:4])

    def getPoseL(self) -> float:
        """Position des Dobots auf der Linearschine abrufen.

        :return: Position in [mm].
        :rtype: float
        """
        m           = DobotMessage(DobotMessageID.GET_POSE_L)
        resp_msg    = self._dev.sendCommand(m)
        return float(resp_msg.params[0])

    def getJointAngles(self) -> DobotJointAngles:
        """Aktuelle Achsenstellungen des Dobot abrufen.

        :return: Die Achsenstellungen von j1-j4, wobei die Zählung beim Endeffektor beginnt.
        :rtype: DobotJointAngles

        :todo: Ist das so?
        """
        resp_msg    = self._dev.sendCommand(DobotMessage(DobotMessageID.GET_POSE))
        # pose_cls = namedtuple("DobotJointAngles", ["j1", "j2", "j3", "j4"])
        # pose = pose_cls(*resp_msg.params[4:])
        return DobotJointAngles(resp_msg.params[4:])


    # MOVEMENTS

    def moveTo(self, x:float, y:float, z:float, r:float=0.0, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        """Bewege Arm zu absoluten Koordinaten.

        :param x: X Koord. in [mm]
        :type x: float
        :param y: Y koord. in [mm]
        :type y: float
        :param z: Z Koord. in [mm]
        :type z: float
        :param r: Rotation in [deg], defaults to 0.0
        :type r: int, optional
        :param wait: Programm blockieren bis Befehl abgearbeitet wurde.
        :type wait: bool, optional
        :param mode: Bewegungsmodus, defaults to DobotPTPMode.MOVL
        :type mode: DobotPTPMode, optional
        """
        m           = DobotMessage(DobotMessageID.SET_PTP_CMD)
        m.params    = (mode.value, x, y, z, r)
        self._dev.sendCommand(m, wait)
        return

    def moveRel(self, dX:float=0.0, dY:float=0.0, dZ:float=0.0, dR:float=0.0, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        """Bewege Arm relative zur aktuellen Position.

        :param dX: rel. X Koord. in [mm], defaults to 0.0
        :type dX: float, optional
        :param dY: rel. Y Koord. in [mm], defaults to 0.0
        :type dY: float, optional
        :param dZ: rel. Z Koord. in [mm], defaults to 0.0
        :type dZ: float, optional
        :param dR: rel. Rotation. in [mm], defaults to 0.0
        :type dR: float, optional
        :param wait: Programm blockieren bis Befehl abgearbeitet wurde.
        :type wait: bool, optional
        :param mode: Bewegungsmodus, defaults to DobotPTPMode.MOVL
        :type mode: int, optional
        """
        pose        = self.getPose()
        self.moveTo(pose.x+dX, pose.y+dY, pose.z+dZ, pose.r+dR, wait, mode)
        return

    def moveToWithL(self, x:float, y:float, z:float, r:float, l:float, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        """Bewege Arm zu absoluten Koordinaten mit Linearschine.

        :param x: X Koord. in [mm]
        :type x: float
        :param y: Y koord. in [mm]
        :type y: float
        :param z: Z Koord. in [mm]
        :type z: float
        :param r: Rotation in [deg]
        :type r: float, optional
        :param l: Position auf der Linearschine in [mm].
        :type l: float
        :param wait: Programm blockieren bis Befehl abgearbeitet wurde.
        :type wait: bool, optional
        :param mode: Bewegungsmodus, defaults to DobotPTPMode.MOVL
        :type mode: DobotPTPMode, optional
        """
        m           = DobotMessage(DobotMessageID.SET_PTP_CMD_WITH_L)
        m.params    = (mode.value, x, y, z, r, l)
        self._dev.sendCommand(m, wait)
        return

    def moveRelWithL(self, dX:float=0.0, dY:float=0.0, dZ:float=0.0, dR:float=0.0, dL:float=0.0, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        pose    = self.getPose()
        poseL   = self.getPoseL()

        self.moveToWithL(pose.x+dX, pose.y+dY, pose.z+dZ, pose.r+dR, poseL+dL, wait, mode)
        return


    def jumpTo(self, x:float, y:float, z:float, r:float, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        """Zur absoluten Position "springen".

        :param x: X Koord. in [mm]
        :type x: float
        :param y: X Koord. in [mm]
        :type y: float
        :param z: X Koord. in [mm]
        :type z: float
        :param r: Drehung [deg]
        :type r: float
        :param wait: Programm blockieren bis Bewegung angeführt wurde, defaults to True
        :type wait: bool, optional
        :param mode: Bewegungsmodus, defaults to DobotPTPMode.MOVJ
        :type mode: int, optional

        :see: Dobot.setJumpHeight()
        """
        # hoch ...
        pose        = self.getPose()
        pose.z      = self.jump_height
        self.moveTo(*pose, wait, mode)
        # ... rüber ...
        pose.x      = x
        pose.y      = y
        pose.r      = r
        #self.moveTo(*pose, wait, mode)
        self.moveTo(*pose, wait, DobotPTPMode.MOVJ) # testen
        # ... runter
        pose.z      = z
        self.moveTo(*pose, wait, mode)
        return

    def jumpRel(self, dX:float=0.0, dY:float=0.0, dZ:float=0.0, dR:float=0.0, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        """Zur relativen Position "springen".

        :param x: X Koord. in [mm]
        :type x: float
        :param y: X Koord. in [mm]
        :type y: float
        :param z: X Koord. in [mm]
        :type z: float
        :param wait: Programm blockieren bis Bewegung angeführt wurde, defaults to True
        :type wait: bool, optional
        :param mode: Bewegungsmodus, defaults to DobotPTPMode.MOVJ
        :type mode: int, optional

        :see: Dobot.setJumpHeight()
        """
        p           = self.getPose()
        self.jumpTo(p.x+dX, p.y+dY, p.z+dZ, p.r+dR, wait=wait, mode=mode)
        return

    def jumpToWithL(self, x:float, y:float, z:float, r:float, l:float, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        # hoch ...
        pose        = self.getPose()

        pose.z      = self.jump_height
        self.moveTo(*pose, wait, mode)
        # ... rüber ...
        pose.x      = x
        pose.y      = y
        pose.r      = r
        self.moveToWithL(*pose, l, wait, DobotPTPMode.MOVJ) # testen
        # ... runter
        pose.z      = z
        self.moveTo(*pose, wait, mode)
        return

    def jumpRelToWithL(self, dX:float, dY:float, dZ:float, dR:float, dL:float, wait:bool=True, mode:DobotPTPMode=DobotPTPMode.MOVJ) -> None:
        # ToDo
        return

    def setR(self, r:float, wait:bool=True) -> None:
        """Rotation des Endeffektor setzen.

        :param r: Winkel on [deg].
        :type r: float
        :param wait: Programm blockieren bis bewegung angeführt wurde, defaults to True
        :type wait: bool, optional
        """
        p           = self.getPose()
        p.r         = r
        self.moveTo(*p, wait=wait)
        return


    def suctionCupOn(self, state:bool) -> None:
        """Vakuumpumpe einschalten.

        :param state: True für Einschalten, False für Ausschalten
        :type state: bool
        """
        m           = DobotMessage(DobotMessageID.SET_ENDEFFECTOR_SUCTION_CUP)
        m.params    = (int(state), 1)
        self._dev.sendCommand(m)
        return


    # EXT HARDWARE

    def setColorSensor(self, isEnabled:bool, port:DobotPort=DobotPort.GP2, version:DobotDeviceVersion=DobotDeviceVersion.VER_V2) -> None:
        """Farbsensor aktivieren.

        :param isEnabled: True wenn aktiviert werden soll, False sonst
        :type isEnabled: bool
        :param port: Port, an den der Farbsensor angeschlossen ist, defaults to DobotPort.GP2
        :type port: DobotPort, optional
        :param version: Sensorversion, defaults to DobotDeviceVersion.VER_V2
        :type version: DobotDeviceVersion, optional

        :raise AssertError: Wenn kein GP Port angegeben wurde.
        """
        assert port.name.startswith("GP"), "Wrong port. Port must start with GP..."
        m           = DobotMessage(DobotMessageID.SET_COLOR_SENSOR)
        m.params    = (int(isEnabled), int(port.id), version.value)
        self._dev.sendCommand(m)
        if isEnabled:
            self.getColor()
        return

    def getColor(self) -> DobotColorSensor:
        """Farbe erkennen.

        :return: Ein DobotColorSensor Objekt, bei dem das Attribut (r, g, b) auf True ist entsprechend der erkannten Farbe
        :rtype: DobotColorSensor
        """
        m           = DobotMessage(DobotMessageID.GET_COLOR_SENSOR)
        resp_msg    = self._dev.sendCommand(m)
        # rgb_cls     = namedtuple("DobotColorSensor", ["r", "g", "b"])
        # return rgb_cls(*list(map(bool, resp_msg.params)))
        return DobotColorSensor(list(map(bool, resp_msg.params)))


    def setIrSwitch(self, isEnabled:bool, port:DobotPort=DobotPort.GP4, version:DobotDeviceVersion=DobotDeviceVersion.VER_V2) -> None:
        """Lichschranke (de)aktivieren.

        :param isEnabled: Ture wenn Lichtschranke aktiviert werden soll. False für das Deaktivieren.
        :type isEnabled: bool
        :param port: Port an dem die Lichschranke angeschlossen ist, defaults to DobotPort.GP4
        :type port: DobotPort, optional
        :param version: Hardwareversion der Lichtschranke, defaults to DobotDeviceVersion.VER_V2
        :type version: DobotDeviceVersion, optional

        :raise AssertError: Wenn kein GP Port angegeben wurde.
        """
        assert port.name.startswith("GP"), "Wrong port. Port must start with GP..."
        m           = DobotMessage(DobotMessageID.SET_IR_SWITCH)
        m.params    = (int(isEnabled), port.id, version.value)
        self._dev.sendCommand(m)
        return

    def getIrSwitch(self, port:DobotPort=DobotPort.GP4) -> bool:
        """Status der Lichtschranke abrufen.

        :param port: Port, an dem die Lichtschranke angeschlossen ist, defaults to DobotPort.GP4
        :type port: DobotPort, optional
        :return: True wenn Lichschranke unterbrochen ist, False sonst.
        :rtype: bool
        """
        assert port.name.startswith("GP"), "Wrong port. Port must start with GP..."
        m           = DobotMessage(DobotMessageID.GET_IR_SWITCH)
        m.params    = (port.id,)
        resp_msg    = self._dev.sendCommand(m)
        return bool(resp_msg.params[0])


    def setConveyorPort(self, port:DobotPort) -> None:
        """Port des Förderbandes festlegen.

        Sollte vor jeder Verwendung der Förderbandes festgelegt werden.
        Das Band muss an einem der vorhandenen Stepper-Ports angeschlossen werden.

        :param port: Port, an dem die
        :type port: DobotPort
        :raise AssertError: Wenn kein STEPPER Port angegeben wurde.
        """
        assert port.name.startswith("STEPPER"), "Wrong port. Port must start with STEPPER..."
        self.conveyor_port = port
        return

    def setConveyorSpeed(self, speed:float) -> None:
        """Geschwindigkeit des Förderband setzen.

        Negative Geschw. lassen das Band rückwärts laufen.

        :param speed: Geschw. in [mm/s]
        :type speed: float
        """
        vel         = float(speed) * self._CONVEYOR_STEP_PER_CIRCLE / self._CONVEYOR_MM_PER_CIRCLE
        m           = DobotMessage(DobotMessageID.SET_EMOTOR)
        m.params    = (self.conveyor_port.id, 1 if vel!=0 else 0, int(vel))
        self._dev.sendCommand(m)
        return


    # I/O

    def setIOMultiplexing(self, port:DobotPort, fnc:DobotIOFunction) -> None:
        """Legt die Funktionen eines IO's fest.

        :param port: EIO-Port dessen Funktion gesetzt werden soll.
        :type port: DobotPort
        :param fnc: Funktion, was der EIO annehmen soll.
        :type fnc: DobotIOFunction

        :raise AssertError: Wenn kein EIO Port angegeben wurde.
        """
        assert port.name.startswith("EIO"), "Wrong port. Port must start with EIO..."
        m           = DobotMessage(DobotMessageID.SET_IO_MULTIPLEXING)
        m.params    = (port.id, fnc.value)
        self._dev.sendCommand(m)
        return

    # def getIOMultiplexing(self) -> tuple:
    #     m           = DobotMessage(DobotMessageID.GET_IO_MULTIPLEXING)
    #     resp_m      = self._dev.sendCommand(m)
    #     return resp_m.params

    def setDO(self, port:DobotPort, val:bool) -> None:
        """Pegel am digitalen Ausgang setzen.

        :param port: EIO Port
        :type port: DobotPort
        :param val: True setzt Port auf HIGH, False auf LOW.
        :type val: bool
        """
        assert port.name.startswith("EIO"), "Wrong port. Port must start with EIO..."
        m           = DobotMessage(DobotMessageID.SET_IO_DO)
        m.params    = (port.id, int(val))
        self._dev.sendCommand(m)
        return

    # def getDO(self) -> tuple:
    #     m           = DobotMessage(DobotMessageID.GET_IO_DO)
    #     resp_msg    = self._dev.sendCommand(m)
    #     return resp_msg.params


    def getDI(self, port:DobotPort) -> bool:
        """Digitalen Eingang lesen.

        :param port: EIO Port der gelesen werden soll.
        :type port: DobotPort
        :return: True wenn EIO auf HIGH ist, False sonst
        :rtype: bool
        """
        assert port.name.startswith("EIO"), "Wrong port. Port must start with EIO..."
        m           = DobotMessage(DobotMessageID.GET_IO_DI)
        m.params    = (port.id, 0)
        resp_msg    = self._dev.sendCommand(m)
        return bool(resp_msg.params[1])


    def getADC(self) -> tuple:
        # ToDo: kein Port angeben?
        # assert port.name.startswith("EIO"), "Wrong port. Port must start with EIO..."
        m           = DobotMessage(DobotMessageID.GET_IO_ADC)
        resp_msg    = self._dev.sendCommand(m)
        return resp_msg.params


    def setPWM(self, port:DobotPort, freq:float, dutyC:float) -> None:
        assert port.name.startswith("EIO"), "Wrong port. Port must start with EIO..."
        m           = DobotMessage(DobotMessageID.SET_IO_PWM)
        m.params    = (port.id, freq, dutyC)
        self._dev.sendCommand(m)
        return

    # def getPWM(self):
    #     m           = DobotMessage(DobotMessageID.GET_IO_PWM)
    #     resp_msg    = self._dev.sendCommand(m)
    #     return resp_msg.params



class DobotMqtt(Dobot):
    def __init__(self, mqtt_client_id:str, mqtt_auth:tuple, mqtt_host:str="localhost", mqtt_port:int=1883, serial_port:str="/dev/ttyUSB0") -> None:
        # Setup MQTT
        self.mqtt_host                  = str(mqtt_host)
        self.mqtt_port                  = int(mqtt_port)
        self.mqtt_auth                  = tuple(mqtt_auth)
        self.mqtt_client_id             = str(mqtt_client_id)
        self.mqtt_topic_prefix          = PurePosixPath(self.mqtt_client_id)
        self.mqtt_allowed_cmds          = ["getPose"]
        self.mqtt_remote_ctrl_enabled   = False
        self._mqtt_msg_q                = LifoQueue()
        self._mqtt_thread               = threading.Thread(target=self._mqttStateLoop, name="DobotMqttStateThread")
        self._mqtt_exit_ev              = threading.Event()

        # setup serial conn
        super().__init__(serial_port)
        self._logger                    = logging.getLogger("{}.{}".format(__name__, self.__class__.__name__))

        # setup mqtt
        self._broker                    = mqtt.Client(client_id=self.mqtt_client_id, clean_session=True)
        self._broker.username_pw_set(str(self.mqtt_auth[0]), str(self.mqtt_auth[1]))
        self._broker.connect(self.mqtt_host, self.mqtt_port, keepalive=30)
        self._broker.loop_start()

        # Callback und Topic festlegen
        self._broker.on_message         = self._onMqttMsg
        self._broker.subscribe("+/msg") # alle Msgs

        self._mqtt_thread.start()

    def close(self) -> None:
        self._mqtt_exit_ev.set()
        self._mqtt_thread.join()

        self.mqttPubMsg("state", "offline")
        self._broker.loop_stop()

        super().close()
        return

    def getColor(self) -> DobotColorSensor:
        c = super().getColor()
        self.mqttPubMsg("colorSens", c.asDict())
        return c

    def getIrSwitch(self, port:DobotPort=DobotPort.GP4) -> bool:
        val = super().getIrSwitch(port)
        self.mqttPubMsg("irSwitch", val)
        return val

    def getDI(self, port: DobotPort) -> bool:
        val = super().getDI(port)
        self.mqttPubMsg("digitalInput/{}".format(port.pname), val)
        return val

    def setDO(self, port: DobotPort, val: bool) -> None:
        super().setDO(port, val)
        self.mqttPubMsg("digitalOutput/{}".format(port.pname), val)


    def mqttSubMsg(self, topic:str) -> None:
        """Topic vom MQTT Broker abonnieren

        :param topic: Topic der abonniert werden soll.
        :type topic: str
        """
        self._broker.subscribe(topic)

    def mqttPubMsg(self, topic:str, payload:Any) -> None:
        """MQTT Nachricht veröffentlichen.

        :param topic: Topic unter dem die Nachricht veröffentlicht werden soll.
        :type topic: str
        :param payload: Nutzdaten der Nachricht.
        :type payload: Any
        """
        if isinstance(payload, dict):
            payload = json.dumps(payload)
        self._broker.publish(str(self.mqtt_topic_prefix / topic), payload)

    def sendMqttMsg(self, msg:str) -> None:
        self._logger.info("send mqtt message: {}".format(msg))
        payload = {
            "from":self.mqtt_client_id,
            "text":msg
        }
        self._broker.publish(str(self.mqtt_topic_prefix / "msg"), json.dumps(payload))

        return

    def mqttMsgAvaiable(self) -> bool:
        return not self._mqtt_msg_q.empty()

    def getMqttMsg(self) -> dict:
        m = self._mqtt_msg_q.get(block=True, timeout=None)
        self._mqtt_msg_q.task_done()

        return m

    def enableRemoteCtrl(self, state:bool) -> None:
        self.mqtt_remote_ctrl_enabled = bool(state)
        self._logger.info("mqtt remote control is {}".format("enabled" if state else "disabled"))

        return


    def _onMqttMsg(self, client:mqtt.Client, userdata:Any, msg:mqtt.MQTTMessage) -> None:
        topic   = PurePosixPath(msg.topic)

        # ToDo testen
        try:
            payload = json.loads(msg.payload)
        except:
            payload = msg.payload

        if payload.get("from", self.mqtt_client_id) == self.mqtt_client_id:
            self._logger.debug("-> drop loopback msg (from = client_id). ")
            return

        self._logger.debug("recv mqtt msg: topic:{}, payload:{}".format(msg.topic, msg.payload))

        # command msg
        if topic.name == "cmd":
            if not self.mqtt_remote_ctrl_enabled:
                return  # remote control nicht erlaubt
            self._handleMqttCmdMsg(payload)

        # nachrichten msg
        elif topic.name == "msg":
            self._logger.info("got mqtt msg: {}".format(payload))
            self._mqtt_msg_q.put(payload)

        return

    def _handleMqttCmdMsg(self, payload:dict) -> None:
        # payload cmd und params extrahieren
        if payload:
            cmd     = payload.get("cmd", None)
            params  = payload.get("params", None)

        if not cmd:
            return # kein cmd übergeben

        if cmd not in self.mqtt_allowed_cmds:
            self._logger.warning("got mqtt command {} witch is not allowed!".format(cmd))
            return  # befehl nicht erlaubt

        fnc = getattr(self, cmd, None)
        if not fnc:
            self._logger.warning("cmd {} not found!".format(cmd))
            return  # befehl nicht vorhanden

        try:
            if params:
                rt = fnc(**params)
            else:
                rt = fnc()
        except:
            rt = "err"

        if not rt:
            return # kein rückgabe wert

        # ToDo Mögliche Datentypen zu json konvertieren
        payload = {
            "from":self.mqtt_client_id,
            "cmd":cmd,
            "value":rt.asDict() if isinstance(rt, (DobotPose, DobotJointAngles, DobotColorSensor, DobotEndEffectorParams)) else rt
        }
        self._broker.publish(str(self.mqtt_topic_prefix / "cmdResp"), json.dumps(payload))

        return

    def _mqttStateLoop(self) -> None:
        while not self._mqtt_exit_ev.is_set():
            if self.getAlarms():
                self.mqttPubMsg("state", "error")
            else:
                self.mqttPubMsg("state", "online")
            time.sleep(3)



if __name__ == "__main__":
    pass