# Installation

Ausgehend einer frischen Windows Installation wird ein Treiber für den im
Dobot befindlichen [USB to Serial Adapter (CP210x)](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers?tab=downloads) benötigt. Unter Linux und Mac wird nichts Weiteres benötigt.

Generell benötigt pyDobot folgende Abhänigekeiten:

- [pyserial](https://pypi.org/project/pyserial/)
- [BitVector](https://pypi.org/project/BitVector/)
- [paho-mqtt](https://pypi.org/project/paho-mqtt/)

Diese werden bei der Installation automatisch mit installiert.

Es gibt drei Methoden das Python Package zu installieren:

1. Package systemweit installieren (klassisch):
    Das Repo herunterladen und mit `python setup.py install` installieren.

2. Package direkt verwenden:
    Repo herunterladen und den `src` Ordner zur `PYTHONPATH` Umgebungsvariable hinzufügen (s. example*.py). Allerdings müssen die Abhänigkeiten per Hand nachinstalliert werden.
    (z.B. aus der `requirements.txt` via `pip install -r requirements.txt`)

3. Direkt von GitLab:
   Sollte ein SSH Key in eurem GitLab-Profil hinterlegt sein, kann direkt das Package systemweit mit ```pip install git+ssh://git@gitlab.gwdg.de:itas/komzet/pydobot.git@main#egg=pyDobot``` installiert werden.
