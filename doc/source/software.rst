Software
########

In diesem Bereich werden alle Module

Dobot
*****

Das Dobotmodul beinhaltet hauptsächlich die Dobot-Klasse, welche die weiteren internen Module zusammenfügt.
Die Dobot-Klasse soll hauptsächlich vom Nutzer verwendet werden.

.. autoclass:: pyDobot.Dobot
    :members:
    :undoc-members:
    :special-members: __init__

.. autoclass:: pyDobot.DobotMqtt
    :members:
    :undoc-members:
    :special-members: __init__

Types
*****

Das Types-Modul umfasst eigene Datentypen.

.. autoclass:: pyDobot.DobotPose
    :members:
    :undoc-members:
    :special-members: __init__

.. autoclass:: pyDobot.DobotJointAngles
    :members:
    :undoc-members:
    :special-members: __init__

.. autoclass:: pyDobot.DobotColorSensor
    :members:
    :undoc-members:
    :special-members: __init__

.. autoclass:: pyDobot.DobotEndEffectorParams
    :members:
    :undoc-members:
    :special-members: __init__

.. autoclass:: pyDobot.DobotPTPJointParams
    :members:
    :undoc-members:
    :special-members: __init__

.. autoclass:: pyDobot.DobotCoordinateParams
    :members:
    :undoc-members:
    :special-members: __init__

.. autoclass:: pyDobot.DobotPTPLParams
    :members:
    :undoc-members:
    :special-members: __init__

Enums
*****

Das Modul enthält Auflistungen, Zuweisungen und Flags.

.. autoclass:: pyDobot.Enums.DobotMessageCtrl
    :members:
    :undoc-members:

.. autoclass:: pyDobot.Enums.DobotMessageID
    :members:
    :undoc-members:

.. autoclass:: pyDobot.DobotAlarmState
    :members:
    :undoc-members:

.. autoclass:: pyDobot.DobotIOFunction
    :members:
    :undoc-members:

.. autoclass:: pyDobot.DobotPTPMode
    :members:
    :undoc-members:

.. autoclass:: pyDobot.DobotDeviceVersion
    :members:
    :undoc-members:

Messages
********

.. autoclass:: pyDobot.Messages.DobotMessage
    :members:
    :undoc-members:
    :special-members: __init__

Communication
*************

Das *Communication* Modul enthält alle Klasse für die reine Kommunikation mit dem Dobot.
Die Klassen (de)serialisieren die Nachrichten vom/zum Dobot über verschiedene Kommunikationswege.

.. autoclass:: pyDobot.Communication.DobotSerial
    :members:
    :undoc-members:
    :special-members: __init__