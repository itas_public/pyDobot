Hardware
########

Im Folgenden werden kurz die Schnittstellen [#dBot_if]_ vom Dobot zusammengefasst.

.. [#dBot_if] Dobot Magician Interface Description (V2), Shenzhen Yuejiang Technology Co., Ltd, 01.07.2019
    Online:
    `Link 1 <https://en.dobot.cn/service/download-center?keyword=&data_type%5B%5D=7>`_,
    `Link 2 <_static/Dobot-Magician-Interface-Description-V2.pdf>`_
    (besucht 19.09.2022)



Schnittstellen
**************

Die einzelnen Pins (bezeichnet mit EIO) des Dobots können unterschiedliche Funktionen haben
(DI, DO, ADC, SW oder PWM). Dieses wird allgemein als Multiplexing bezeichnet. Entsprechnet der
Hardware, sind einige EIO's in Gruppen zusammengefaßt (z.B. GP1-GP5).

Bevor ein EIO verwendet werden kann, sollte die Funktion zuvor mit ``setIOMultiplexing()`` des EIO's
festgelegt werden.

.. note::
    SW
        Schalter zum Ein-/Ausschalten.
    PWN
        Pulse Width Modulation zur Ansteuerung von Servomotoren.
    ADC
        Analog Digital Converter zum Messen von Spannungen.
    DI, DO
        Digital Input/Output

.. figure:: ./_static/hardware-base_interface2.png
    :align: center
    :scale: 75

    Schnittstellen auf der Rückseite des Dobots.

+-------------+-------+-----------------------+-----------+-------+
| Bezeichnung | Pin   | Eingang               | Ausgang   | Port  |
+=============+=======+=======================+===========+=======+
| SW1         | VALUE |                       | 12V 1A    | EIO16 |
+-------------+-------+-----------------------+-----------+-------+
| SW1         | GND   |                       |           |       |
+-------------+-------+-----------------------+-----------+-------+
| SW2         | PUMP  |                       | 12V 1A    | EIO17 |
+-------------+-------+-----------------------+-----------+-------+
| SW2         | GND   |                       |           |       |
+-------------+-------+-----------------------+-----------+-------+
| Stepper1    | 5_*   |                       | 12V 0.9V  |       |
+-------------+-------+-----------------------+-----------+-------+
| Stepper2    | 4_*   |                       | 12V 0.9A  |       |
+-------------+-------+-----------------------+-----------+-------+
| GP1         | ADC   | 3.3V / [#f1]_ 5V 20mA |           | EIO12 |
+-------------+-------+-----------------------+-----------+-------+
| GP1         | PWM   |                       | 3.3V 20mA | EIO11 |
+-------------+-------+-----------------------+-----------+-------+
| GP1         | REV   |                       | 5V 1A     | EIO10 |
+-------------+-------+-----------------------+-----------+-------+
| GP1         | GND   |                       |           |       |
+-------------+-------+-----------------------+-----------+-------+
| GP2         | ADC   | 3.3V / 5V [#f1]_ 20mA | 3.3V 20mA | EIO15 |
+-------------+-------+-----------------------+-----------+-------+
| GP2         | PWM   | 3.3V / 5V 20mA        | 3.3V 10mA | EIO14 |
+-------------+-------+-----------------------+-----------+-------+
| GP2         | REV   |                       | 5V 1A     | EIO13 |
+-------------+-------+-----------------------+-----------+-------+
| GP2         | GND   |                       |           |       |
+-------------+-------+-----------------------+-----------+-------+

.. [#f1] 5V als max. Eingangsspannung im ADC Modus.

.. figure:: ./_static/hardware-uart_interface.png
    :align: center
    :scale: 60

    UART Schnittstelle auf Rückseite des Dobots.

.. figure:: ./_static/hardware-arm_interface2.png
    :align: center
    :scale: 60

    Schnittstelle am Dobot Arm.

+-------------+----------+----------------+-----------+------+
| Bezeichnung | Pin      | Eingang        | Ausgang   | Port |
+=============+==========+================+===========+======+
| Analog      | Temp     |                |           | EIO1 |
+-------------+----------+----------------+-----------+------+
| Analog      | GND      |                |           |      |
+-------------+----------+----------------+-----------+------+
| SW4         | FAN_12V  |                | 12V 1A    | EIO2 |
+-------------+----------+----------------+-----------+------+
| SW4         | GND      |                |           |      |
+-------------+----------+----------------+-----------+------+
| SW3         | HEAT_12V |                | 12V 3A    | EIO3 |
+-------------+----------+----------------+-----------+------+
| SW3         | GND      |                |           |      |
+-------------+----------+----------------+-----------+------+
| GP5         | ADC      | 3.3V / 5V 20mA |           | EIO5 |
+-------------+----------+----------------+-----------+------+
| GP5         | PWM      |                | 3.3V 20mA | EIO4 |
+-------------+----------+----------------+-----------+------+
| GP5         | 5V       |                | 5V 1A     |      |
+-------------+----------+----------------+-----------+------+
| GP5         | GND      |                |           |      |
+-------------+----------+----------------+-----------+------+
| GP4         | ADC      | 3.3V / 5V 20mA |           | EIO7 |
+-------------+----------+----------------+-----------+------+
| GP4         | PWM      |                | 3.3V 20mA | EIO6 |
+-------------+----------+----------------+-----------+------+
| GP4         | 5V       |                | 5V 1A     |      |
+-------------+----------+----------------+-----------+------+
| GP4         | GND      |                |           |      |
+-------------+----------+----------------+-----------+------+
| GP3         | ADC      | 3.3V / 5V 20mA |           | EIO9 |
+-------------+----------+----------------+-----------+------+
| GP3         | PWM      |                | 3.3V 20mA | EIO8 |
+-------------+----------+----------------+-----------+------+
| GP3         | 5V       |                | 5V 1A     |      |
+-------------+----------+----------------+-----------+------+
| GP3         | GND      |                |           |      |
+-------------+----------+----------------+-----------+------+



Beispiele
=========

In ``DobotPort`` und ``DobotIOFunktion`` sind die einzelnen Ports und Funktionen definiert.
Den Ports müssen jeweils mit ``setIOMultiplexing`` eine Funktion zugewiesen werden.

.. code-block:: python
    :linenos:

    from pyDobot import Dobot
    from pyDobot import DobotPort
    from pyDobot import DobotIOFunction

    # Verbindung zum Dobot herstellen
    dBot = Dobot("/dev/ttyUSB0")

    # Verwendung von EIO4 als digitalen Ausgang
    dBot.setIOMultiplexing(DobotPort.EIO4, DobotIOFunction.IO_FUNC_DO)

    # Setzen vom EIO4 auf High (3.3V)
    dBot.setDO(DobotPort.EIO4, True)

    # ...



Förderband
**********


Linearschine
************
