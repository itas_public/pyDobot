#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# intern
import sys
import time
# own
sys.path.insert(0, "src")           # Diese Zeile ist nur notwendig, wenn pyDobot nicht installiert wurde.
from pyDobot import DobotMqtt, DobotPort, DobotIOFunction

if __name__ == "__main__":
    dBot = DobotMqtt(
        mqtt_host       = "localhost",
        mqtt_auth       = ("mqttuser", "mqttpasswd"),
        mqtt_client_id  = "myDobot",
        serial_port     = "/dev/ttyUSB0"
    )

    dBot.clearAlarms()
    dBot.setEndEffector(DobotMqtt.ENDEFFECTOR_SUCTIONCUP)

    dBot.setColorSensor(True)
    dBot.setIrSwitch(True)
    dBot.setIOMultiplexing(DobotPort.EIO5, DobotIOFunction.IO_FUNC_DI)

    print("press Crtl+c to abort...")

    while True:
        try:
            dBot.getColor()
            dBot.getIrSwitch()
            dBot.getDI(DobotPort.EIO5)
            time.sleep(0.5)
        except KeyboardInterrupt:
            break

    dBot.setColorSensor(False)
    dBot.setIrSwitch(False)

    dBot.close()
    sys.exit()
