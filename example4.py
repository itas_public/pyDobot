#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# intern
import sys
# own
sys.path.insert(0, "src")           # Diese Zeile ist nur notwendig, wenn pyDobot nicht installiert wurde.
from pyDobot import Dobot, DobotIOFunction, DobotPort

if __name__ == "__main__":
    dBot = Dobot("/dev/ttyUSB0")

    dBot.setIOMultiplexing(DobotPort.EIO5, DobotIOFunction.IO_FUNC_DI)

    print(dBot.getDI(DobotPort.EIO5))

    dBot.close()
    sys.exit()
