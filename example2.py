#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# intern
import sys
import time
# own
sys.path.insert(0, "src")               # Diese Zeile ist nur notwendig, wenn pyDobot nicht installiert wurde.
from pyDobot import Dobot, DobotPort

if __name__ == "__main__":
    dBot = Dobot("/dev/ttyUSB0")
    dBot.setEndEffector(Dobot.ENDEFFECTOR_SUCTIONCUP)
    dBot.setConveyorPort(DobotPort.STEPPER1)

    dBot.setConveyorSpeed(50)
    time.sleep(0.5)
    dBot.setConveyorSpeed(0)

    dBot.setColorSensor(True, DobotPort.GP2)
    print(dBot.getColor())

    dBot.setIrSwitch(True, DobotPort.GP5)
    print(dBot.getIrSwitch())

    dBot.close()
    sys.exit()
