#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# intern
import sys
import time
# own
sys.path.insert(0, "src")       # Diese Zeile ist nur notwendig, wenn pyDobot nicht installiert wurde.
from pyDobot import Dobot

if __name__ == "__main__":
    dBot = Dobot("/dev/ttyUSB0")
    dBot.setEndEffector(Dobot.ENDEFFECTOR_SUCTIONCUP)
    dBot.home()

    p = dBot.getPose()
    print(p)

    dBot.moveRel(dY=-50)
    dBot.moveRel(dY=100)

    dBot.suctionCupOn(True)
    time.sleep(0.3)
    dBot.suctionCupOn(False)

    dBot.close()

    sys.exit()
