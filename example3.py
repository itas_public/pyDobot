#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

# intern
import sys
# own
sys.path.insert(0, "src")           # Diese Zeile ist nur notwendig, wenn pyDobot nicht installiert wurde.
from pyDobot import Dobot

if __name__ == "__main__":
    dBot = Dobot("/dev/ttyUSB0")
    dBot.setDeviceWithL(True)

    p = dBot.getPose()

    dBot.moveToWithL(*p, l=500)
    dBot.moveToWithL(*p, l=0)

    dBot.close()
    sys.exit()
