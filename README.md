# pyDoBot (v0.10)

Eine reine Python API Implementierung für den Dobot Magician.

Die einzelne Änderungen zwischen den Versionen kann in der [CHANGELOG.md](CHANGELOG.md) nachgeschlagen werden.

Eine kurze Installationsanweisung in der [INSTALL.md](INSTALL.md) hinterlegt.

Getestet unter Python 3.9+.

## ToDo

Funktionen:
* [x] Basis Befehle
* [x] Farbsensor
* [x] Lichtschranke (noch testen)
* [x] Förderband
* [x] Linearschiene
* [x] I/O's (noch testen)
+ [x] MQTT
* [ ] Definition eines Workspaces