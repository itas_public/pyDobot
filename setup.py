#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
from pathlib import Path
from setuptools import setup, find_packages

req_fp          = Path().cwd() / "requirements.txt"
req_links_fp    = Path().cwd() / "dependency_links.txt"

required = []
with req_fp.open() as f:
    required = f.read().splitlines()

dep_links = []
if req_links_fp.is_file():
    with req_links_fp.open() as f:
        dep_links = f.read().splitlines()

setup(
    name="pyDobot",
    description="Pure python 3 api for the dobot magician.",
    version="0.10",
    author="Y. Korte-Wagner",
    author_email="yves.korte-wagner@jade-hs.de",
    url="https://gitlab.gwdg.de/itas/komzet/pydobot",
    dependency_links=dep_links,
    install_requires = required,
    package_dir={"":"src"},
    packages=["pyDobot"]
)